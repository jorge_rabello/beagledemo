package br.jorgerabellodev.beagledemo.controller

import br.com.zup.beagle.widget.layout.ScreenBuilder
import br.jorgerabellodev.beagledemo.service.MyService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MyController(private val service: MyService) {
   @GetMapping("/screen")
    fun getScreen() : ScreenBuilder = service.getMyScreen()
}