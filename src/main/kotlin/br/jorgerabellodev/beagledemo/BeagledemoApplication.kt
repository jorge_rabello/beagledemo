package br.jorgerabellodev.beagledemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BeagledemoApplication

fun main(args: Array<String>) {
	runApplication<BeagledemoApplication>(*args)
}
