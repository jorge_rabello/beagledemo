package br.jorgerabellodev.beagledemo.configurations

import br.com.zup.beagle.constants.BEAGLE_EXPOSED_HEADERS
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class CorsConfiguration : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedOrigins("meusite.com.br")
                .allowedMethods("GET, PUT, POST")
                .allowedHeaders("Cache-Control")
                .exposedHeaders(*BEAGLE_EXPOSED_HEADERS)
    }
}