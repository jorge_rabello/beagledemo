## Simple Beagle Demo

This is a simple beagle framework demo and experimentation based on this docs:

Documentation
* https://docs.usebeagle.io/v1.10/overview/

Main Site 
* https://usebeagle.io/

Github and Source Code 
* https://github.com/ZupIT/beagle


Medium Article 
* https://medium.com/@Uziasf/beagle-criando-sua-primeira-tela-com-server-driven-ui-97d3df9b90e7?p=28a7f8236557

Youtube Videos 
* https://www.youtube.com/playlist?list=PLkX9oUrQ1ev6dcIQYGUBiE1CBihev0gdG

## To execute

To run this project just execute

```shell
./mvnw spring-boot:run
```

Then access http://localhost:8080/screen
